import {getSinglePageTemplate} from '/js/templates';
import {fetchCharacterData} from '/js/fetch';

export const displayCharacterData = rootElement => {
    const resetCharacter = () => {
        rootElement.innerHTML = '';
    };

    const loadCharacter = id => {
        fetchCharacterData(id).then(response => {
            const html = response.data.results.map(({id, name, description, thumbnail}) => (
                getSinglePageTemplate({
                    id,
                    name,
                    description,
                    thumbnail: `${thumbnail.path}.${thumbnail.extension}`,
                })
            ));

            rootElement.insertAdjacentHTML('beforeend', html);
        });
    };

    return {resetCharacter, loadCharacter};
};
