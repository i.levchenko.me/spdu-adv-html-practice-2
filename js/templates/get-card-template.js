import {domParser} from '/js/utils';

export const getCardTemplate = ({id, name, thumbnail, description, onClick}) => {
    const card = domParser.parseFromString(
        `<div class="card">
            <p>${name}</p>
            <img src="${thumbnail}" alt="${name}" class="card-image">
            <p>${description}</p>
        </div>`,
        'text/html'
    ).body.firstChild;

    if (onClick) {
        card.addEventListener('click', event => {
           onClick(event, id);
        });
    }

    return card;
};
