import {pathToRegexp, match} from 'path-to-regexp';

const router = () => {
    let routes = [];

    const getMatched = url => routes.find(({regexp}) => regexp.test(url));

    const getParams = (path, url) => match(path, { decode: decodeURIComponent })(url).params;

    window.addEventListener('popstate', () => {
        const matched = getMatched(window.location.pathname);

        if (matched) {
            matched.handler(getParams(matched.path, window.location.pathname));
        }
    });

    const addRoute = (path, handler) => {
        const regexp = pathToRegexp(path);

        routes = [...routes, {path, regexp, handler}];
    };

    const addRoutes = routes => {
        routes.map(route => {
            addRoute(route.path, route.handler);
        });

        const matched = getMatched(window.location.pathname);

        if (matched) {
            matched.handler(getParams(matched.path, window.location.pathname));
        }
    };

    const removeRoute = path => {
        routes = routes.filter(route => route.path !== path);
    };

    const goTo = url => {
        const matched = getMatched(url);

        if (matched) {
            window.history.pushState({}, '', `${window.location.origin}/${url.replace(/^\//, '')}`);
            matched.handler(getParams(matched.path, url));
        }
    };

    return {
        addRoute,
        addRoutes,
        removeRoute,
        goTo
    };
};

export const {addRoute, addRoutes, goTo, removeRoute} = router();
