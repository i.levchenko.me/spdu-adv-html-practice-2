export {displayCharactersData} from './display-characters-data';
export {displayCharacterData} from './display-character-data';
export {displayComicsData} from './display-comics-data';
export {displayComicData} from './display-comic-data';
