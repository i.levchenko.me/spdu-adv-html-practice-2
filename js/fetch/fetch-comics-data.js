import {BASE_URL} from '/js/constants';
import {
    titleQuery,
    titleStartsWithQuery,
    modifiedSinceQuery,
    limitQuery,
    orderByQuery,
    formatQuery,
    noVariantsQuery,
    queryBuilder,
    offsetQuery
} from '/js/utils/query-builder';

export const fetchComicsData = queries => {
    const {title, titleStartsWith, modifiedSince, limit, orderBy, order, page, format, noVariants} = queries;
    const ascendingOrder = orderBy && order === 'ASC' ? orderBy : '';
    const descendingOrder = orderBy && order === 'DSC' ? `-${orderBy}` : '';

    const queriesArray = [
        limitQuery(limit),
        offsetQuery(limit * page),
        titleQuery(title),
        titleStartsWithQuery(titleStartsWith),
        modifiedSinceQuery(modifiedSince),
        formatQuery(format),
        noVariantsQuery(noVariants),
        orderByQuery(ascendingOrder || descendingOrder)
    ];

    const url = queryBuilder(`${BASE_URL}/comics`, queriesArray);

    return fetch(url).then(response => (
        response.json()
    ));
};
