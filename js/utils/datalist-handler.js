export const datalistHandler = datalistElement => {
    const addOptions = items => {
        const options = items.map(item => `<option data-value="${1}">${item}</option>`).join('');

        datalistElement.insertAdjacentHTML('beforeend', options);
    };

    const removeOptions = () => {
        datalistElement.innerHTML = '';
    };

    return {addOptions, removeOptions};
};
